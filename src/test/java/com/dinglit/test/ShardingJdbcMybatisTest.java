package com.dinglit.test;

import com.dinglit.domain.StudentDO;
import com.dinglit.domain.UserDO;
import com.dinglit.service.StudentReadService;
import com.dinglit.service.StudentWriteService;
import com.dinglit.service.UserReadService;
import com.dinglit.service.UserWriteService;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-08-25
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-database.xml",
        "classpath*:spring-sharding.xml" })
public class ShardingJdbcMybatisTest {

	@Autowired
    public UserReadService userReadService;

	@Autowired
    public UserWriteService userWriteService;

	@Autowired
    public StudentReadService studentReadService;

	@Autowired
    public StudentWriteService studentWriteService;

    @Test
    public void testUserInsert() {
        for(int i=32;i<3200000;i++){
            UserDO u = new UserDO();
            u.setUserId(i);
            u.setAge(i);
            u.setName("github"+i);
            Assert.assertEquals(userWriteService.insert(u), true);
        }
    }

    @Test
    public void testStudentInsert() {

        for(int i=0;i<3200000;i++){
            StudentDO student = new StudentDO();
            student.setStudentId(i);
            student.setAge(i);
            student.setName("hehe"+i);
            Assert.assertEquals(studentWriteService.insert(student), true);
        }
    }

    @Test
    public void testFindAll(){
        List<UserDO> users = userReadService.findAll();
        if(null != users && !users.isEmpty()){
            for(UserDO u :users){
                System.out.println(u);
            }
        }
    }

    @Test
    public void testSQLIN(){
        List<UserDO> users = userReadService.findByUserIds(Arrays.asList(13,16));
        if(null != users && !users.isEmpty()){
            for(UserDO u :users){
                System.out.println(u);
            }
        }
    }

    @Test
    public void testTransactionTestSucess(){
        userReadService.transactionTestSucess();
    }

    @Test(expected = IllegalAccessException.class)
    public void testTransactionTestFailure() throws IllegalAccessException{
        userReadService.transactionTestFailure();
    }


}
