package com.dinglit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinglit.dao.StudentDAO;
import com.dinglit.domain.StudentDO;
import com.dinglit.service.StudentWriteService;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-08-05
 *
 */
@Service
public class StudentWriteServiceImpl implements StudentWriteService{

    @Autowired
    public StudentDAO studentDAO;

    public boolean insert(StudentDO student) {
        return studentDAO.insert(student) > 0 ? true : false;
    }

}
