package com.dinglit.service.impl;

import com.dinglit.dao.StudentDAO;
import com.dinglit.dao.UserDAO;
import com.dinglit.domain.StudentDO;
import com.dinglit.domain.UserDO;
import com.dinglit.service.UserReadService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-08-15
 *
 */
@Service
public class UserReadServiceImpl implements UserReadService {
   
	@Resource
    public UserDAO userDAO;

    @Resource
    public StudentDAO studentDAO;

    public List<UserDO> findAll() {
        return userDAO.findAll();
    }

    public List<UserDO> findByUserIds(List<Integer> ids) {
        return userDAO.findByUserIds(ids);
    }

    @Transactional(propagation=Propagation.REQUIRED)
    public void transactionTestSucess() {
        UserDO u = new UserDO();
        u.setUserId(13);
        u.setAge(25);
        u.setName("war3 1.27");
        userDAO.insert(u);

        StudentDO student = new StudentDO();
        student.setStudentId(21);
        student.setAge(21);
        student.setName("hehe");
        studentDAO.insert(student);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void transactionTestFailure() throws IllegalAccessException {
        UserDO u = new UserDO();
        u.setUserId(13);
        u.setAge(25);
        u.setName("war3 1.27 good");
        userDAO.insert(u);

        StudentDO student = new StudentDO();
        student.setStudentId(21);
        student.setAge(21);
        student.setName("hehe1");
        studentDAO.insert(student);
        throw new IllegalAccessException();
    }
}
