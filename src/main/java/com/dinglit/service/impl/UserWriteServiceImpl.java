package com.dinglit.service.impl;

import com.dinglit.dao.UserDAO;
import com.dinglit.domain.UserDO;
import com.dinglit.service.UserWriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-08-15
 *
 */
@Service
public class UserWriteServiceImpl implements UserWriteService {

    @Autowired
    public UserDAO userDAO;

    public boolean insert(UserDO u) {
        return userDAO.insert(u) > 0 ? true :false;
    }
}
