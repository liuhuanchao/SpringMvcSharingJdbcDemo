package com.dinglit.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dinglit.dao.StudentDAO;
import com.dinglit.domain.StudentDO;
import com.dinglit.service.StudentReadService;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-08-05
 *
 */
@Service
public class StudentReadServiceImpl implements StudentReadService{
    
	@Resource
    public StudentDAO studentDAO;
	
    public List<StudentDO> findAll() {
        return studentDAO.findAll();
    }
    
}
