package com.dinglit.service;

import java.util.List;

import com.dinglit.domain.StudentDO;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-07-30
 *
 */
public interface StudentReadService {
	
	List<StudentDO> findAll();
}
