package com.dinglit.service;

import com.dinglit.domain.UserDO;

import java.util.List;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-08-05
 *
 */
public interface UserReadService {

    public List<UserDO> findAll();

    public List<UserDO> findByUserIds(List<Integer> ids);

    public void transactionTestSucess();

    public void transactionTestFailure() throws IllegalAccessException;
}
