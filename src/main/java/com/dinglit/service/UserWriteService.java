package com.dinglit.service;

import com.dinglit.domain.UserDO;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-08-05
 *
 */
public interface UserWriteService {

    public boolean insert(UserDO userDO);
}
