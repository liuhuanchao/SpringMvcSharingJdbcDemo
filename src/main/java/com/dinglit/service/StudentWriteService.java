package com.dinglit.service;

import com.dinglit.domain.StudentDO;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-07-30
 *
 */
public interface StudentWriteService {

    boolean insert(StudentDO studentDO);
}
