package com.dinglit.dao.impl;

import com.dinglit.dao.BaseDao;
import com.dinglit.dao.UserDAO;
import com.dinglit.domain.UserDO;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-07-30
 *
 */
@Component
public class UserDaoImpl extends BaseDao implements UserDAO {

    public Integer insert(UserDO userDO) {
        int row = insert("UserDaoImpl_insert", userDO);
        return row;
    }

    public List<UserDO> findAll() {
        List<UserDO> list = (List<UserDO>) selectList("UserDaoImpl_findAll");
        return list;
    }

    public List<UserDO> findByUserIds(List<Integer> userIds) {
        List<UserDO> list = (List<UserDO>) selectList("UserDaoImpl_findByUserIds",userIds);
        return list;
    }
}
