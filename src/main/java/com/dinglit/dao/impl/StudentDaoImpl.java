package com.dinglit.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.dinglit.dao.BaseDao;
import com.dinglit.dao.StudentDAO;
import com.dinglit.domain.StudentDO;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-07-30
 *
 */
@Component
public class StudentDaoImpl extends BaseDao implements StudentDAO {

	public Integer insert(StudentDO studentDO) {
		int row = insert("StudentDaoImpl_insert", studentDO);
		return row;
	}

	public List<StudentDO> findAll() {
		List<StudentDO> list = (List<StudentDO>) selectList("StudentDaoImpl_findAll");
		return list;
	}

	public List<StudentDO> findByStudentIds(List<Integer> studentIds) {
		List<StudentDO> list = (List<StudentDO>) selectList("StudentDaoImpl_findByStudentIds",studentIds);
		return list;
	}

}
