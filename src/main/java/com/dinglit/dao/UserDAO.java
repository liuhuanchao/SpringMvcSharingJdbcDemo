package com.dinglit.dao;

import com.dinglit.domain.UserDO;

import java.util.List;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-07-30
 *
 */
public interface UserDAO {

    Integer insert(UserDO userDO);

    List<UserDO> findAll();

    List<UserDO> findByUserIds(List<Integer> userIds);
}
