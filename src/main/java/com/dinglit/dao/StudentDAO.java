package com.dinglit.dao;

import java.util.List;

import com.dinglit.domain.StudentDO;

/**
 * @Email  740970978
 * @author liuhuanchao
 * @since  2017-07-30
 *
 */
public interface StudentDAO {

    Integer insert(StudentDO studentDO);

    List<StudentDO> findAll();

    List<StudentDO> findByStudentIds(List<Integer> studentIds);
}
